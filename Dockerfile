FROM ubuntu

LABEL "project"="heal"
LABEL "author"="kokila"

RUN apt update
RUN apt install apache2 -y

CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]

EXPOSE 8001

WORKDIR /var/www/html/
VOLUME /var/log/apache2/

ADD final.tar.gz /var/www/html/
